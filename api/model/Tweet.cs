﻿using System;
using System.Collections.Generic;
using System.Text;

namespace me.allardyce.twitter.model
{
    public class Tweet
    {
        public string id { get; set; }
        public string user { get; set; }
        public string content { get; set; }
        public string created { get; set; }
        public string partition { get; set; } = "default";
    }    
}
