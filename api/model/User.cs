﻿using System;
using System.Collections.Generic;
using System.Text;

namespace me.allardyce.twitter.model
{
    public class User
    {
        public string id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string partition { get; set; } = "default";
    }    
}
