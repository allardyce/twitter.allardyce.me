using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Azure.WebJobs.Extensions;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

using me.allardyce.twitter.model;
using me.allardyce.twitter.helper;
using System.Collections.Generic;

namespace me.allardyce.twitter.function
{
    public static class login
    {
        [FunctionName("login")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] User user,
            [CosmosDB("twitter", "users", ConnectionStringSetting = "twitterConnection", SqlQuery = "SELECT top 1 * FROM c WHERE c.username = {username}")] IEnumerable<User> lookupUsers,
            ILogger log)
        {

            if (string.IsNullOrEmpty(user.username) || string.IsNullOrEmpty(user.password)) return new BadRequestObjectResult(new { message = "Username and password required for login." });

            if (!lookupUsers.Any()) return new BadRequestObjectResult(new { message = "Username not found." });

            User lookupUser = lookupUsers.First();

            bool passwordValid = SecurePasswordHasher.Verify(user.password, lookupUser.password);

            if (passwordValid == false) return new BadRequestObjectResult(new { message = "Username or password incorrect." });

            string token = JWTService.Generate(lookupUser.id);

            return new OkObjectResult(new { token, id = lookupUser.id });

        }
    }
}
