using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using me.allardyce.twitter.helper;
using me.allardyce.twitter.model;

namespace me.allardyce.twitter.function
{
    public static class tweetid
    {

        [FunctionName("tweet-id")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "tweet/{id}")] HttpRequest req,
            [CosmosDB("twitter", "tweets", ConnectionStringSetting = "twitterConnection", Id = "{id}", PartitionKey = "default")] Tweet tweet,
            ILogger log)
        {
            return new OkObjectResult(tweet);
        }
    }
}
