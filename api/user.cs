using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using me.allardyce.twitter.helper;
using System.Collections.Generic;
using me.allardyce.twitter.model;
using System.Linq;

namespace me.allardyce.twitter.function
{

    public static class user
    {
        [FunctionName("user")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "user/{id}")] HttpRequest req,
            [CosmosDB("twitter", "users", ConnectionStringSetting = "twitterConnection", Id = "{id}", PartitionKey = "default")] User user,
            ILogger log)
        {
            if (user == null) return new BadRequestObjectResult(new { message = "User not found." });

            return new OkObjectResult(new {
                user.id,
                user.username
            });
        }
    }
}
