using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using me.allardyce.twitter.helper;
using me.allardyce.twitter.model;

namespace me.allardyce.twitter.function
{
    public static class tweet
    {
        [FunctionName("tweet")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] HttpRequest req,
            [CosmosDB("twitter", "tweets", ConnectionStringSetting = "twitterConnection")] IAsyncCollector<Tweet> tweetOut,
            ILogger log)
        {
            if (!req.Headers.ContainsKey("JWT")) return new UnauthorizedObjectResult(new { message = "JWT missing." });
            if (!JWTService.Validate(req.Headers["JWT"], out string id)) return new UnauthorizedObjectResult(new { message = "JWT invalid." });

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);

            string content = data.content;

            if (string.IsNullOrEmpty(content)) return new BadRequestObjectResult(new { message = "Missing tweet content." });

            if (content.Length > 280) return new BadRequestObjectResult(new { message = "Tweet content too long." });

            Tweet tweet = new Tweet {
                id = Guid.NewGuid().ToString(),
                user = id,
                content = content,
                created = DateTime.UtcNow.ToString()
            };

            await tweetOut.AddAsync(tweet);

            return new OkObjectResult(tweet);
        }
    }
}
