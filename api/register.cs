using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using me.allardyce.twitter.model;
using Microsoft.Azure.Documents.Client;
using System.Linq;
using me.allardyce.twitter.helper;
using System.Collections.Generic;

namespace me.allardyce.twitter.function
{
    public static class register
    {
        [FunctionName("register")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] User user,
            [CosmosDB("twitter", "users", ConnectionStringSetting = "twitterConnection", SqlQuery = "SELECT top 1 * FROM c WHERE c.username = {username}")] IEnumerable<User> lookupUsers,
            [CosmosDB("twitter", "users", ConnectionStringSetting = "twitterConnection")] IAsyncCollector<User> usersOut,
            ILogger log)
        {

            if (string.IsNullOrEmpty(user.username) || string.IsNullOrEmpty(user.password)) return new BadRequestObjectResult(new { message = "Username and password required to register." });

            if (lookupUsers.Any()) return new BadRequestObjectResult(new { message = "Username already registered." });

            user.password = SecurePasswordHasher.Hash(user.password);

            user.id = Guid.NewGuid().ToString();

            await usersOut.AddAsync(user);

            string token = JWTService.Generate(user.id);

            return new OkObjectResult(new { token, id = user.id });
        }
    }
}
