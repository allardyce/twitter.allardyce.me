﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using me.allardyce.twitter.model;

namespace me.allardyce.twitter.helper
{
    class JWTService
    {
        private const string SecretKey = "secretkeykittens";

        public static string Generate(string id, int expires = 10080)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

            DateTime now = DateTime.UtcNow;

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                    new Claim("id", id)
                }),
                Expires = now.AddMinutes(expires),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecretKey)), SecurityAlgorithms.HmacSha256),
            };

            SecurityToken stoken = tokenHandler.CreateToken(tokenDescriptor);

            string token = tokenHandler.WriteToken(stoken);

            return token;

        }

        public static ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

                JwtSecurityToken jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null) return null;


                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecretKey))
                };

                SecurityToken securityToken;

                ClaimsPrincipal principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);

                return principal;
            }
            catch (Exception)
            {
                //should write log
                return null;
            }
        }

        public static bool Validate(string token, out string id)
        {
            id = null;

            ClaimsPrincipal simplePrinciple = GetPrincipal(token);

            if (simplePrinciple == null) return false;

            ClaimsIdentity identity = simplePrinciple.Identity as ClaimsIdentity;

            if (identity == null) return false;

            if (!identity.IsAuthenticated) return false;

            Claim idClaim = identity.Claims.Where(c => c.Type == "id").FirstOrDefault();
            id = idClaim?.Value;

            if (string.IsNullOrEmpty(id)) return false;

            return true;
        }

    }
}
