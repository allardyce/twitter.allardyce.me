using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using me.allardyce.twitter.model;
using System.Collections.Generic;

namespace api
{
    public static class tweets
    {
        [FunctionName("tweets")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)] HttpRequest req,
            [CosmosDB("twitter", "tweets", ConnectionStringSetting = "twitterConnection", SqlQuery = "SELECT * FROM c ORDER BY c.created DESC")] IEnumerable<Tweet> lookupTweets,
            ILogger log)
        {

            return new OkObjectResult(lookupTweets.ToArray());
        }
    }
}
