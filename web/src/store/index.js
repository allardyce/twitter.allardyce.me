import { configureStore } from '@reduxjs/toolkit';

import { reducer as usersReducer } from './users';

const store = configureStore({
    reducer: {
        users: usersReducer
    }
});

export default store;
