import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import axios from 'axios';

const getUserDetails = createAsyncThunk('users/fetch', async (id, { dispatch, getState }) => {
    let state = getState();

    if (state.users[id]) return;

    await dispatch(actions.userLoading(id));

    const { data } = await axios.get(`/api/user/${id}`);

    return data;
});

const users = createSlice({
    name: 'users',

    initialState: {},

    reducers: {
        userLoading(state, { payload }) {
            if (state[payload]) return state;
            return { [payload]: { loading: true }, ...state };
        }
    },

    extraReducers: {
        [getUserDetails.fulfilled]: (state, { payload }) => {
            if (!payload) return state;
            state[payload.id] = payload;
        }
    }
});

export const { actions, reducer } = users;

actions.getUserDetails = getUserDetails;
