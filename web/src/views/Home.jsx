import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Tweets from '../components/Tweets';

const Home = () => {
    const [tweets, setTweets] = useState([]);

    useEffect(() => {
        axios.get('/api/tweets').then(({ data }) => {
            setTweets(data);
        });
    }, []);

    return (
        <div className="view-home">
            <div className="row">
                <h1 className="title is-1">Home</h1>
                <hr />
            </div>
            <div className="row">
                <Tweets tweets={tweets} />
            </div>
        </div>
    );
};

export default Home;
