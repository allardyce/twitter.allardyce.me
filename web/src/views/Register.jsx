import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

const Register = ({ setUser }) => {
    const history = useHistory();

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const submit = e => {
        e.preventDefault();

        setError(null);

        if (password !== passwordConfirm) {
            setError('Passwords do not match');
            return;
        }

        setLoading(true);

        axios
            .post('/api/register', { username, password })
            .then(({ data }) => {
                setUser(data);
                history.push('/');
            })
            .catch(e => {
                if (e.response && e.response.data && e.response.data.message) {
                    setError(e.response.data.message);
                }
                setLoading(false);
            });
    };

    return (
        <div className="view-register">
            <div className="row">
                <h1 className="title is-1 has-text-centered">Register</h1>
            </div>
            <div className="login row">
                {error && (
                    <div className="row">
                        <div className="message is-danger">
                            <div className="message-body">{error}</div>
                        </div>
                    </div>
                )}
                <form onSubmit={submit}>
                    <div className="field">
                        <label className="label">Username</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Username" onInput={e => setUsername(e.target.value)} />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Password</label>
                        <div className="control">
                            <input className="input" type="password" placeholder="Password" onInput={e => setPassword(e.target.value)} />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Repeat Password</label>
                        <div className="control">
                            <input className="input" type="password" placeholder="Password" onInput={e => setPasswordConfirm(e.target.value)} />
                        </div>
                    </div>
                    <div className="field">
                        <div className="control">
                            <button className={`button is-primary is-fullwidth ${loading && 'is-loading'}`}>Login</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default Register;
