import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';

const Logout = ({ setUser }) => {
    const history = useHistory();

    useEffect(() => {
        setUser({});
        localStorage['twitter.allardyce.me'] = '{}';
        history.push('/');
    }, [history, setUser]);

    return <div className="view-logout">Logging out..</div>;
};

export default Logout;
