import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';

import User from '../components/User';
import Tweets from '../components/Tweets';

const UserView = () => {
    let { id } = useParams();

    const [tweets, setTweets] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        axios.get(`/api/tweets/user/${id}`).then(({ data }) => {
            setTweets(data);
            setLoading(false);
        });
    }, [id]);

    return (
        <div className="view-user">
            <div className="row">
                <h2 className="title is-2">
                    <User id={id} />
                    's tweets
                </h2>

                {loading ? <div className="is-loading"></div> : <Tweets tweets={tweets} />}
            </div>
        </div>
    );
};

export default UserView;
