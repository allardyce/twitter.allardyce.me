import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

const Tweet = ({ user }) => {
    const history = useHistory();

    const [content, setContent] = useState('');
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const submit = e => {
        e.preventDefault();

        setError(null);

        setLoading(true);

        axios
            .post(
                '/api/tweet',
                { content },
                {
                    headers: { JWT: user.token }
                }
            )
            .then(({ data }) => {
                history.push(`/user/${user.id}`);
            })
            .catch(e => {
                if (e.response && e.response.data && e.response.data.message) {
                    setError(e.response.data.message);
                }
                setLoading(false);
            });
    };

    return (
        <div className="view-register">
            <div className="row">
                <h1 className="title is-1 has-text-centered">New Tweet</h1>
            </div>
            <div className="login row">
                {error && (
                    <div className="row">
                        <div className="message is-danger">
                            <div className="message-body">{error}</div>
                        </div>
                    </div>
                )}
                <form onSubmit={submit}>
                    <div className="field">
                        <div className="control">
                            <textarea className="textarea" type="textarea" placeholder="Send a message" onInput={e => setContent(e.target.value)} />
                        </div>
                    </div>
                    <div className="field">
                        <div className="control">Characters: {content.length} / 280</div>
                    </div>
                    <div className="field">
                        <div className="control">
                            <button className={`button is-primary is-fullwidth ${loading && 'is-loading'}`}>Tweet</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default Tweet;
