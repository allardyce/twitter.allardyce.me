import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { actions } from '../store/users';

const User = ({ id, users, getUserDetails }) => {
    // const dispatch = useDispatch();

    useEffect(() => {
        if (!users[id]) {
            getUserDetails(id);
        }
    }, [getUserDetails, id, users]);

    return (
        <Link className={'has-text-strong ' + (users[id] && users[id].loading && 'is-loading')} to={`/user/${id}`}>
            {users[id] && users[id].username}
        </Link>
    );
};

const mapStateToProps = state => state;

const mapDispatchToProps = actions;

export default connect(mapStateToProps, mapDispatchToProps)(User);
