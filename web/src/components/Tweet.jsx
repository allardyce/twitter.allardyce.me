import React from 'react';

import User from './User';

const Tweet = ({ tweet }) => {
    return (
        <div className="tweet message is-white">
            <div className="message-body">
                <div className="tweet-info">
                    <div className="columns">
                        <div className="column">
                            <User id={tweet.user} />
                        </div>
                        <div className="column has-text-right">{tweet.created}</div>
                    </div>
                </div>
                <div className="tweet-content">{tweet.content}</div>
            </div>
        </div>
    );
};

export default Tweet;
