import React from 'react';

import Tweet from './Tweet';

const Tweets = ({ tweets }) => {
    return (
        <div className="tweets">
            {!tweets.length && (
                <div className="message is-warning">
                    <div className="message-body">No tweets found.</div>
                </div>
            )}
            {tweets.map(tweet => (
                <Tweet key={tweet.id} tweet={tweet} />
            ))}
        </div>
    );
};

export default Tweets;
