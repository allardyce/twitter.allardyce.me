import React from 'react';
import { Link } from 'react-router-dom';

const Header = ({ user }) => {
    return (
        <nav className="navbar is-primary is-spaced" role="navigation" aria-label="main navigation">
            <div className="container">
                <div className="navbar-brand">
                    <Link className="navbar-item site-title" to="/">
                        Twitter | Allardyce
                    </Link>

                    <div role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </div>
                </div>

                <div className="navbar-menu">
                    <div className="navbar-end">
                        {user.id && (
                            <div className="navbar-item">
                                <Link className="button is-rounded is-info" to="/tweet">
                                    New Tweet
                                </Link>
                            </div>
                        )}
                        {user.id && (
                            <div className="navbar-item has-dropdown is-hoverable">
                                <div className="navbar-link">Settings</div>

                                <div className="navbar-dropdown">
                                    <Link className="navbar-item" to={`/user/${user.id}`}>
                                        Profile
                                    </Link>
                                    <hr className="navbar-divider" />
                                    <Link className="navbar-item" to="/logout">
                                        Logout
                                    </Link>
                                </div>
                            </div>
                        )}
                        {!user.id && (
                            <div className="navbar-item">
                                <div className="buttons">
                                    <Link className="button is-primary" to="/register">
                                        <strong>Sign up</strong>
                                    </Link>
                                    <Link className="button is-light" to="/login">
                                        Log in
                                    </Link>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </nav>
    );
};

export default Header;
