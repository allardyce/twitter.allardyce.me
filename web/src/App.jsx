import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Header from './components/Header';

import Login from './views/Login';
import Logout from './views/Logout';
import Register from './views/Register';
import Home from './views/Home';
import User from './views/User';
import Tweet from './views/Tweet';

const App = () => {
    const [user, setUser] = useState({});

    // Retrieve and store user data / JWT in local storage...
    useEffect(() => {
        if (!localStorage['twitter.allardyce.me']) localStorage['twitter.allardyce.me'] = '{}';

        let localStore = JSON.parse(localStorage['twitter.allardyce.me']);

        if (user.id) {
            localStorage['twitter.allardyce.me'] = JSON.stringify(user);
        } else if (!user.id && localStore.id) {
            setUser(localStore);
        }
    }, [user]);

    return (
        <div className="app">
            <Router>
                <Header user={user} />

                <div className="container">
                    <div className="row">
                        <Switch>
                            <Route path="/logout">
                                <Logout setUser={setUser} />
                            </Route>
                            <Route path="/tweet">
                                <Tweet user={user} />
                            </Route>
                            <Route path="/user/:id">
                                <User />
                            </Route>
                            <Route path="/login">
                                <Login setUser={setUser} />
                            </Route>
                            <Route path="/register">
                                <Register setUser={setUser} />
                            </Route>
                            <Route path="/">
                                <Home />
                            </Route>
                        </Switch>
                    </div>
                </div>
            </Router>
        </div>
    );
};

export default App;
