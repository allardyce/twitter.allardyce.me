# build react frontend
FROM node:11-alpine as build-frontend

WORKDIR /app

COPY ./web/package.json .
COPY ./web/yarn.lock .

RUN yarn install

COPY ./web .

RUN yarn build

################################################################################################3

FROM nginx:alpine as production-stage

COPY ./nginx.conf /etc/nginx/nginx.conf

COPY --from=build-frontend /app/build /usr/share/nginx/html

EXPOSE 80