# Introduction

A very simple clone of Twitter to practice using Azure Functions

# Getting Started

## API

1. Open api.sln using Visual Studio
2. Press F5 to run functions locally
3. Use Azure Cosmos DB Emulator to develop fully locally
    1. Update the local.settings.json `twitterConnection` to the correct local DB account

## Web

0. `cd web`
1. `yarn install`
1. `yarn start`
